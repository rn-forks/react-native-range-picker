"use strict";
import React, { Component } from "react";
import PropTypes from "prop-types";
import {
  Text,
  View,
  FlatList,
  StyleSheet,
  Button,
  TouchableOpacity
} from "react-native";
import Month from "./Month";
// import styles from './styles';
import moment from "moment";

export default class RangeDatepicker extends Component {
  constructor(props) {
    super(props);
    this.state = {
      startDate: props.startDate && moment(props.startDate, "YYYYMMDD"),
      untilDate: props.untilDate && moment(props.untilDate, "YYYYMMDD"),
      availableDates: props.availableDates || null
    };

    this.onSelectDate = this.onSelectDate.bind(this);
    this.onReset = this.onReset.bind(this);
    this.handleConfirmDate = this.handleConfirmDate.bind(this);
    this.handleClose = this.handleClose.bind(this);
    this.handleRenderRow = this.handleRenderRow.bind(this);
  }

  async componentDidMount() {
    let { isHistorical, monthsToAdd } = this.props;
    if (!isHistorical && monthsToAdd && monthsToAdd.length > 0) {
      let list = this.getMonthStack();
      let wait = new Promise(resolve => setTimeout(resolve, 300));
      await wait.then(() => {
        let index = 0;
        let todayMonth = moment(new Date()).format("YYYYMM");
        list.map((month, i) => {
          if (month === todayMonth) {
            index = i;
          }
        });

        this.flatList.scrollToIndex({ index: index, animated: false });
      });
    }
  }

  static defaultProps = {
    initialMonth: "",
    dayHeadings: ["S", "M", "T", "W", "T", "F", "S"],
    maxMonth: 12,
    buttonColor: "green",
    buttonContainerStyle: {},
    showReset: true,
    showClose: true,
    ignoreMinDate: false,
    isHistorical: false,
    onClose: () => {},
    onSelect: () => {},
    onConfirm: () => {},
    placeHolderStart: "",
    placeHolderUntil: "",
    selectedBackgroundColor: "green",
    selectedTextColor: "white",
    todayColor: "green",
    startDate: "",
    untilDate: "",
    minDate: "",
    maxDate: "",
    infoText: "",
    infoStyle: { color: "#fff", fontSize: 13 },
    infoContainerStyle: {
      marginRight: 20,
      paddingHorizontal: 20,
      paddingVertical: 5,
      backgroundColor: "green",
      borderRadius: 20,
      alignSelf: "flex-end"
    },
    showSelectionInfo: true,
    showButton: true
  };

  static propTypes = {
    initialMonth: PropTypes.string,
    dayHeadings: PropTypes.arrayOf(PropTypes.string),
    availableDates: PropTypes.arrayOf(PropTypes.string),
    maxMonth: PropTypes.number,
    buttonColor: PropTypes.string,
    buttonContainerStyle: PropTypes.object,
    startDate: PropTypes.string,
    untilDate: PropTypes.string,
    minDate: PropTypes.string,
    maxDate: PropTypes.string,
    showReset: PropTypes.bool,
    resetStyle: PropTypes.object,
    resetText: PropTypes.string,
    showClose: PropTypes.bool,
    closeStyle: PropTypes.object,
    closeText: PropTypes.string,
    ignoreMinDate: PropTypes.bool,
    isHistorical: PropTypes.bool,
    onClose: PropTypes.func,
    onSelect: PropTypes.func,
    onConfirm: PropTypes.func,
    placeHolderStart: PropTypes.string,
    placeHolderUntil: PropTypes.string,
    selectedBackgroundColor: PropTypes.string,
    selectedTextColor: PropTypes.string,
    todayColor: PropTypes.string,
    infoText: PropTypes.string,
    infoStyle: PropTypes.object,
    infoContainerStyle: PropTypes.object,
    showSelectionInfo: PropTypes.bool,
    showButton: PropTypes.bool,
    confirmDateStyle: PropTypes.object,
    confirmDateTitle: PropTypes.string
  };

  componentWillReceiveProps(nextProps) {
    this.setState({ availableDates: nextProps.availableDates });
  }

  onSelectDate(date) {
    let startDate = null;
    let untilDate = null;
    const { availableDates } = this.state;

    if (this.state.startDate && !this.state.untilDate) {
      if (
        date.format("YYYYMMDD") < this.state.startDate.format("YYYYMMDD") ||
        this.isInvalidRange(date)
      ) {
        startDate = date;
      } else if (
        date.format("YYYYMMDD") > this.state.startDate.format("YYYYMMDD")
      ) {
        startDate = this.state.startDate;
        untilDate = date;
      } else {
        startDate = null;
        untilDate = null;
      }
    } else if (!this.isInvalidRange(date)) {
      startDate = date;
    } else {
      startDate = null;
      untilDate = null;
    }

    this.setState({ startDate, untilDate });
    this.props.onSelect(startDate, untilDate);
  }

  isInvalidRange(date) {
    const { startDate, untilDate, availableDates } = this.state;

    if (availableDates && availableDates.length > 0) {
      //select endDate condition
      if (startDate && !untilDate) {
        for (
          let i = startDate.format("YYYYMMDD");
          i <= date.format("YYYYMMDD");
          i = moment(i, "YYYYMMDD")
            .add(1, "days")
            .format("YYYYMMDD")
        ) {
          if (
            availableDates.indexOf(i) == -1 &&
            startDate.format("YYYYMMDD") != i
          )
            return true;
        }
      }
      //select startDate condition
      else if (availableDates.indexOf(date.format("YYYYMMDD")) == -1)
        return true;
    }

    return false;
  }

  getMonthStack() {
    let res = [];
    const { maxMonth, initialMonth, isHistorical } = this.props;
    let initMonth = moment();
    if (initialMonth && initialMonth != "")
      initMonth = moment(initialMonth, "YYYYMM");

    for (let i = 0; i < maxMonth; i++) {
      res.push(
        !isHistorical
          ? initMonth
              .clone()
              .add(i, "month")
              .format("YYYYMM")
          : initMonth
              .clone()
              .subtract(i, "month")
              .format("YYYYMM")
      );
    }

    if (
      !isHistorical &&
      this.props.monthsToAdd &&
      this.props.monthsToAdd.length > 0
    ) {
      this.props.monthsToAdd.forEach(month => {
        res.unshift(month);
      });
    }

    return res;
  }

  onReset() {
    this.setState({
      startDate: null,
      untilDate: null
    });

    this.props.onSelect(null, null);
  }

  handleConfirmDate() {
    this.props.onConfirm &&
      this.props.onConfirm(this.state.startDate, this.state.untilDate);
  }

  handleClose() {
    this.props.onClose &&
      this.props.onClose(this.state.startDate, this.state.untilDate);
  }

  handleRenderRow(month, index) {
    const {
      selectedBackgroundColor,
      selectedTextColor,
      todayColor,
      ignoreMinDate,
      minDate,
      maxDate
    } = this.props;
    let { availableDates, startDate, untilDate } = this.state;

    if (availableDates && availableDates.length > 0) {
      availableDates = availableDates.filter(function(d) {
        if (d.indexOf(month) >= 0) return true;
      });
    }

    return (
      <Month
        onSelectDate={this.onSelectDate}
        startDate={startDate}
        untilDate={untilDate}
        availableDates={availableDates}
        minDate={minDate ? moment(minDate, "YYYYMMDD") : minDate}
        maxDate={maxDate ? moment(maxDate, "YYYYMMDD") : maxDate}
        ignoreMinDate={ignoreMinDate}
        dayProps={{ selectedBackgroundColor, selectedTextColor, todayColor }}
        month={month}
      />
    );
  }

  camelize(string) {
    return string.replace(/\w\S*/g, function(txt) {
      return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
    });
  }

  render() {
    return (
      <View
        style={{
          backgroundColor: "#fff",
          zIndex: 1000,
          alignSelf: "center",
          width: "100%",
          flex: 1
        }}
      >
        {this.props.showClose || this.props.showReset ? (
          <View
            style={{
              flexDirection: "row",
              justifyContent: "space-between",
              padding: 20,
              paddingBottom: 10
            }}
          >
            {this.props.showClose && (
              <TouchableOpacity
                style={[{ fontSize: 20 }, this.props.closeStyle]}
                onPress={this.handleClose}
              >
                <Text style={this.props.closeTextStyle}>{`${
                  this.props.closeText
                }`}</Text>
              </TouchableOpacity>
            )}
            {this.props.showReset && (
              <TouchableOpacity
                style={[{ fontSize: 20 }, this.props.resetStyle]}
                onPress={this.onReset}
              >
                <Text style={this.props.resetTextStyle}>{`${
                  this.props.resetText
                }`}</Text>
              </TouchableOpacity>
            )}
          </View>
        ) : null}
        {this.props.showSelectionInfo ? (
          <View
            style={{
              flexDirection: "row",
              justifyContent: this.props.placeHolderGeneral
                ? "center"
                : "space-between",
              paddingHorizontal: 20,
              paddingVertical: this.props.placeHolderGeneral ? 20 : 0,
              paddingBottom: this.props.placeHolderGeneral ? 20 : 5,
              alignItems: "center"
            }}
          >
            <View style={{ flex: 1 }}>
              <Text
                style={{
                  fontSize: this.state.startDate ? 20 : 18,
                  color: "#666"
                }}
              >
                {this.state.startDate && this.state.untilDate
                  ? this.camelize(
                      moment(this.state.startDate).format("MMM DD YYYY")
                    )
                  : this.props.placeHolderStart !== ""
                  ? this.props.placeHolderStart
                  : this.props.placeHolderGeneral
                  ? ""
                  : ""}
              </Text>
            </View>

            <View style={{}}>
              <Text
                style={{
                  fontSize:
                    this.props.placeHolderGeneral && !this.state.untilDate
                      ? 20
                      : 20
                }}
              >
                {this.props.placeHolderGeneral &&
                this.state.untilDate &&
                this.state.startDate
                  ? "/"
                  : this.props.placeHolderGeneral &&
                    !this.state.startDate &&
                    !this.state.untilDate
                  ? "Seleccionar fecha"
                  : this.props.placeHolderGeneral &&
                    this.state.startDate &&
                    !this.state.untilDate
                  ? this.camelize(
                      moment(this.state.startDate).format("MMMM DD YYYY")
                    )
                  : ""}
              </Text>
            </View>

            <View style={{ flex: 1 }}>
              <Text
                style={{
                  fontSize: this.props.placeHolderGeneral ? 20 : 34,
                  color: "#666",
                  textAlign: "right"
                }}
              >
                {this.state.untilDate
                  ? this.camelize(
                      moment(this.state.untilDate).format("MMM DD YYYY")
                    )
                  : this.props.placeHolderUntil !== ""
                  ? this.props.placeHolderUntil
                  : this.props.placeHolderGeneral
                  ? ""
                  : ""}
              </Text>
            </View>
          </View>
        ) : null}

        {this.props.infoText != "" && (
          <View style={this.props.infoContainerStyle}>
            <Text style={this.props.infoStyle}>{this.props.infoText}</Text>
          </View>
        )}
        <View style={styles.dayHeader}>
          {this.props.dayHeadings.map((day, i) => {
            return (
              <Text style={{ width: "14.28%", textAlign: "center" }} key={i}>
                {day}
              </Text>
            );
          })}
        </View>
        <FlatList
          style={{ flex: 1 }}
          ref={ref => {
            this.flatList = ref;
          }}
          data={this.getMonthStack()}
          renderItem={({ item, index }) => {
            return this.handleRenderRow(item, index);
          }}
          keyExtractor={(item, index) => index.toString()}
          showsVerticalScrollIndicator={false}
        />

        {this.props.showButton ? (
          <View style={[styles.buttonWrapper, this.props.buttonContainerStyle]}>
            <TouchableOpacity
              onPress={this.handleConfirmDate}
              style={this.props.confirmDateStyle}
            >
              <Text style={this.props.confirmTextStyle}>{`${
                this.props.confirmDateTitle
              }`}</Text>
            </TouchableOpacity>
          </View>
        ) : null}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  dayHeader: {
    flexDirection: "row",
    borderBottomWidth: 1,
    paddingBottom: 10,
    paddingTop: 10
  },
  buttonWrapper: {
    paddingVertical: 10,
    paddingHorizontal: 15,
    backgroundColor: "white",
    borderTopWidth: 1,
    borderColor: "#ccc",
    alignItems: "stretch"
  }
});
